require "bundler/capistrano"
require "rvm/capistrano"
require 'capistrano/ext/multistage'

load "deploy/assets"

set :application, "SocialLoginApp"
set :repository,  "git@bitbucket.org:pavana21/socialloginapplication.git"

set :scm, :git
set :user, "deploy"
set :deploy_to, "/apps/SocialLoginApp"
set :use_sudo, false

current_branch = $1 if `git branch` =~ /\* (\S+)\s/

set :branch, 'master'
set :stages, %w(production)
set :default_stage, "production"
# set :whenever_command, "bundle exec whenever"
# set :whenever_environment, defer { stage }
# require "whenever/capistrano"

set :chmod755, 'app config db lib public vendor script script/* public/ disp* log/'
set :runner, 'rails'

namespace :deploy do
  after 'deploy:setup', 'deploy:basic_setup'
  before "deploy:assets:precompile", "deploy:symlink_shared"
  after 'deploy:update', 'deploy:cleanup'
  after "deploy", "deploy:migrate"
  
  task :stop, :roles => :app do

      invoke_command "cd #{current_path};bundle exec thin stop -C config/thin.yml"
      invoke_command "service thin stop"
  end

  desc "Custom AceMoney deployment: start."
  task :start, :roles => :app do

      invoke_command "cd #{current_path};bundle exec thin start -C config/thin.yml"
      invoke_command "service thin start"
  end

  # Need to define this restart ALSO as 'cap deploy' uses it
  # (Gautam) I dont know how to call tasks within tasks.
  desc "Custom AceMoney deployment: restart."
  task :restart, :roles => :app do

      invoke_command "cd #{current_path};bundle exec thin stop -C config/thin.yml"
      invoke_command "service thin stop"
      invoke_command "cd #{current_path};bundle exec thin start -C config/thin.yml"
      invoke_command "service thin start"
  end

  task :basic_setup do
    # run "mkdir -p #{shared_path}/uploads"
    run "mkdir -p #{shared_path}/config"
    # run "mkdir -p #{deploy_to}/shared/solr/data"
  end

  task :symlink_shared, :roles => :app do
    # run "ln -nfs #{shared_path}/solr/data #{release_path}/solr/data"
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    # run "ln -nfs #{shared_path}/uploads #{release_path}/public/uploads"
  end

  # task :stop, :roles => :app do
  #   run "kill -QUIT `cat #{deploy_to}/shared/pids/unicorn.pid`; true"
  # end
  # 
  # task :start, :roles => :app do
  #   run "mkdir -p #{shared_path}/sockets && ln -s #{shared_path}/sockets #{release_path}/tmp/sockets"
  #   run "cd #{release_path} && bundle exec unicorn -Dc #{release_path}/config/deploy/#{stage}/unicorn.rb -E #{rails_env}"
  # end
  # 
  # task :restart, :roles => :app do
  #   stop
  #   start
  # end
end

namespace :db do
  task :create, :roles => :app do
    run "cd #{current_path}; bundle exec rake db:create RAILS_ENV=#{rails_env}"
  end
  
  task :setup, :roles => :app do
    run "cp -f #{current_path}/config/deploy/#{stage}/database.yml #{shared_path}/config/database.yml"
  end
end

# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
# 
# role :web, "your web-server here"                          # Your HTTP server, Apache/etc
# role :app, "your app-server here"                          # This may be the same as your `Web` server
# role :db,  "your primary db-server here", :primary => true # This is where Rails migrations will run
# role :db,  "your slave db-server here"
# 
# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end