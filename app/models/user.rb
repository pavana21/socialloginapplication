class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable

  attr_accessible :email, :password, :password_confirmation, :remember_me, :provider, :uid, :username
  
  def self.find_for_facebook_oauth(auth, signed_in_resource=nil)
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = User.create(username:auth.extra.raw_info.name,
                           provider:auth.provider,
                           uid:auth.uid,
                           email:auth.info.email,
                           password:Devise.friendly_token[0,20]
                           )
    end
    user
  end
  
  def self.find_for_google_oauth2(access_token, signed_in_resource=nil)
    data = access_token.info
    user = User.where(:email => data["email"]).first

    unless user
      user = User.create(username: data["name"],
    		   email: data["email"],
    		   password: Devise.friendly_token[0,20]
    		  )
    end
    user
  end
  
  def self.find_for_twitter_oauth(auth, signed_in_resource=nil)
    nickname = auth.extra.raw_info.screen_name
    email = "#{nickname}" + "@twitter.com"
    user = User.where(:provider => auth.provider, :uid => auth.uid).first
    unless user
      user = User.create(username: auth.extra.raw_info.name,
                          provider:auth.provider,
                          email: email,
                          uid:auth.uid,
                          password:Devise.friendly_token[0,20]
                            )
    end
    user
  end
  
end
